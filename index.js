// console.log("Hello World!");




// NO RETURN SUM



function twoNumbers(num1, num2) {
	console.log("Displayed sum of " + num1 + " and " + num2);

}

twoNumbers(5, 15);




// COMPUTE ADDITION


function oneAndTwo(numOne, numTwo) {
	console.log(numOne + numTwo);
}

oneAndTwo (5, 15);





// NO RETURN DIFFERENCE



function minusOne(numA, numB) {
	console.log("Displayed difference of " + numA + " and " + numB);

}

minusOne(20, 5);



// COMPUTE SUBTRACTION


function minusTwo(numD, numE) {
	console.log(numD - numE);
}

minusTwo (20, 5);





// --------------------------------------





// FUNCTION WITH RETURN MULTIPLICATION



function multiplyNum(m1, m2) {
	console.log("The product of " + m1 + " and " + m2);
	return m1 * m2;

}

let resultMultiply = multiplyNum(50, 10);
console.log(resultMultiply);






// FUNCTION WITH RETURN MULTIPLICATION



function quotientA(quo1, quo2) {
	console.log("The quotient of " + quo1 + " and " +quo2);
	return quo1 / quo2;
}

let resultDivision = quotientA(50, 10);
console.log(resultDivision);








// --------------------------------------





// FUNCTION WITH RETURN CIRCLE FROM A RADIUS


function circleArea(radius) {
	console.log("The result of getting the area of a circle with " + radius + " radius:");
	
	const pie = 3.1416

	return pie * radius ** 2; 
}

let area = circleArea(15);
console.log(area);








// --------------------------------------





// FUNCTION WITH RETURN TO GET THE TOTAL AVERAGE OF FOUR #'s


function getAverage(n1, n2, n3, n4) {
	console.log("The average of " + n1 + "," + n2 + "," + n3 + " and " + n4 + ":");
	return (n1 + n2 + n3 + n4) / 4; 
}

let averageVar = getAverage(20, 40, 60, 80);
console.log(averageVar)










// --------------------------------------





// FUNCTION TO CHECK THE PERCENTAGE IF YOU PASS




let score1 = 38
let score2 = 50


console.log("Is " + score1 + "/" + score2 + " a passing score?");


// -----------


function isPassingScore(score, total) {
	let passingScore = ((score/total)*100) > 75;
	return passingScore;
}


let isPassed = isPassingScore(38, 50);
console.log(isPassed);








